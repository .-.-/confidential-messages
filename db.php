<?php

require_once "config.php";

function open_database() {
	global $database, $db_username, $db_password;
	return new PDO($database, $db_username, $db_password);
}

function save_message($values) {
	$db = open_database();
	$keys = array_keys($values);
	$stmt = $db->prepare("INSERT INTO comments (" . implode(', ', $keys) . ") VALUES (:" . implode(', :', $keys) . ")");
	return $stmt->execute($values);
}

function read_messages() {
	$db = open_database();
	return $db->query("SELECT * FROM comments", PDO::FETCH_NAMED);
}

function encrypt_values($values) {
	global $openpgp_userid;

	$encrypted = [];
	foreach ($values as $name => $value) {
		$process = proc_open(['gpg', '-r', $openpgp_userid, "-e"], [ 0 => ['pipe', 'r'], 1 => ['pipe', 'w']], $pipes);
		fwrite($pipes[0], $value);
		fclose($pipes[0]);
		$encrypted[$name] = stream_get_contents($pipes[1]);
		fclose($pipes[1]);
		$retval = proc_close($process);
		if ($retval) {
			return ["return $retval for $name"];
		}
	}
	return $encrypted;
}

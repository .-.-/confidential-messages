$(document).ready(function() {
  if (typeof mailvelope !== 'undefined') {
    setup();
  } else {
    window.addEventListener('mailvelope', setup, false);
  }
  window.addEventListener('mailvelope-disconnect', function(event) {
    $('#newVersion').val(event.detail.version);
    $('#disconnectModal').modal('show');
  }, false);
  initAddDomain();
});

function initAddDomain() {
  $('#addDomain').on('click', function() {
    $('body').append('<iframe style="display: none;" src="https://api.mailvelope.com/authorize-domain/?api=true" />')
  });
}

function setup() {
  var keyring = null;
  var automaticids = 0;
  mailvelope.getKeyring().then(function(keyr) {
    keyring = keyr;
  });

  $('.encrypted').on('click', function() {
    var message = "-----BEGIN PGP MESSAGE-----\n\n" + this.dataset.encrypted + "=\n-----END PGP MESSAGE-----";
    var options = {};

    if (!this.id) this.id="automaticid" + ++automaticids;
    mailvelope.createDisplayContainer('#' + this.id, message, keyring, options);
  });
}

<?php

# Fill out here your database details
$database = 'sqlite:/tmp/db.sqlite3';
$db_username = '';
$db_password = '';

$openpgp_userid = '<user@domain.example>';

# Fields that are asked to the user. The names MUST BE identifiers!
# Do not change after initial setup (or fixup the database manually) 
$fields = [ 'Name' => 'text', 'Email' => 'email', 'Message' => 'textarea' ];

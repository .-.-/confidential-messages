<?php
	require_once "config.php";
	require_once "db.php";
?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Send your confidential message here</title>
	<style>
		label { padding-right: 1em; }
	</style>
</head>
<body>
<?php
	if ($_SERVER["REQUEST_METHOD"] == 'POST') {
		$values = [];
		foreach ($fields as $field => $type) {
			if (!isset($_POST[$field]) || $_POST[$field] == '') {
				echo '<p class="error">Field <em>' . htmlspecialchars($field) . "</em> shall be filled.</p>\n";
			} else {
				$values[$field] = $_POST[$field];
			}
		}
		if (count($fields) == count($values)) {
			$values = encrypt_values($values);
			if (save_message($values)) {
				echo "<p>Message saved successfully</p>\n";
			} else {
				echo "<p class=\"error\">Error saving message</p>\n";
			}
		}
	}
?>
	<form action="submit.php" method="post">
<?php
	foreach ($fields as $field => $type) {
		if ($type == 'textarea') {
			echo "\t\t<p><label for=\"$field\">" . htmlspecialchars("$field:") . "</label><textarea required id=\"$field\" name=\"$field\"></textarea></p>\n";
		} else {
			echo "\t\t<p><label for=\"$field\">" . htmlspecialchars("$field:") . "</label><input required type=\"$type\" id=\"$field\" name=\"$field\" value=\"\"></p>\n";
		}
	}
?>
		<input type="submit">
	</form>
</body>
</html>

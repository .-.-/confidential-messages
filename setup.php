<?php

require_once "db.php";

$create = "CREATE TABLE comments (";
$create .= " 	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,";
foreach ($fields as $field => $type) {
	$create .= " 	$field BLOB,";
}
$create = rtrim($create, ",") . ");";

if (open_database()->exec($create) === false) {
	die("Error creating table");
} else {
	echo "Setup successful\n";
}

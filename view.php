<?php

# Page where the site owner can read the comments
# Access is assumed to be restricted by the web server 

require_once "db.php";

?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Send your confidential message here</title>
	<style>
		#addDomain { text-align: right; }
		.message { border: 1px solid; padding-left: 0.5em; margin: 1em; }
		.value { padding-left: 10em; }
		.encrypted:empty::after { padding-left: 5em; font-style: italic; content: "Encrypted content. Click to view"; }
	</style>
	<script src="/javascript/jquery/jquery.min.js"></script>
	<script src="fields.js"></script>
</head>
<body>
	<p id="addDomain"><a href="#">Enable domain on Mailvelope</a></p>
<?php
	$keys = array_keys($fields);
	foreach (read_messages() as $message) {
		echo '<div class="message">';
		foreach ($keys as $field) {
			echo '<p class="field">' . htmlspecialchars($field) . ": </p>\n";
			echo '<p class="value encrypted" data-encrypted="' . htmlspecialchars(base64_encode($message[$field])) . "\"></p>\n";
		}
		echo "</div>\n\n";
	}
?>
</body>
</html>
